package assertions.pet;

/**
 * Entry point for assertions of different data types. Each method in this class is a static factory for the
 * type-specific assertion objects.
 */
@javax.annotation.Generated(value = "assertj-assertions-generator")
public class Assertions {

    /**
     * Creates a new instance of <code>{@link CategoryAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static CategoryAssert assertThat(models.pet.Category actual) {
        return new CategoryAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link PetModelAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static PetModelAssert assertThat(models.pet.PetModel actual) {
        return new PetModelAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link PetModelBuilderAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static PetModelBuilderAssert assertThat(models.pet.PetModel.Builder actual) {
        return new PetModelBuilderAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link PetModelStatusAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static PetModelStatusAssert assertThat(models.pet.PetModel.Status actual) {
        return new PetModelStatusAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link PetNotFoundModelAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static PetNotFoundModelAssert assertThat(models.pet.PetNotFoundModel actual) {
        return new PetNotFoundModelAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link PetNotFoundModelBuilderAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static PetNotFoundModelBuilderAssert assertThat(models.pet.PetNotFoundModel.Builder actual) {
        return new PetNotFoundModelBuilderAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link TagsItemAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static TagsItemAssert assertThat(models.pet.TagsItem actual) {
        return new TagsItemAssert(actual);
    }

    /**
     * Creates a new <code>{@link Assertions}</code>.
     */
    protected Assertions() {
        // empty
    }
}
