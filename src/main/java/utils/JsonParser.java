package utils;

import com.sun.codemodel.JCodeModel;
import org.jsonschema2pojo.Jackson1Annotator;
import org.jsonschema2pojo.SchemaGenerator;
import org.jsonschema2pojo.SchemaMapper;
import org.jsonschema2pojo.SchemaStore;
import org.jsonschema2pojo.rules.RuleFactory;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class JsonParser {
    private static void parseJsonFromFileToClass(String fileName) {
        URL source = null;
        try {
            source = new URL("file:///C:\\Users\\skammer\\workspace\\io.swagger.perstore\\src\\main\\resources\\jsons\\" + fileName);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        JCodeModel codeModel = new JCodeModel();
        MyConfig generationConfig = new MyConfig();
        RuleFactory ruleFactory = new RuleFactory(generationConfig, new Jackson1Annotator(generationConfig), new SchemaStore());
        SchemaGenerator generator = new SchemaGenerator();

        SchemaMapper mapper = new SchemaMapper(ruleFactory, generator);
        mapper.generate(codeModel, "Board", "", source);

        try {
            codeModel.build(new File("C:\\Users\\skammer\\workspace\\io.swagger.perstore\\src\\main\\java\\models\\pet"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        parseJsonFromFileToClass("pet");
    }
}
