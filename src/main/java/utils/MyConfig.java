package utils;

import org.jsonschema2pojo.DefaultGenerationConfig;
import org.jsonschema2pojo.SourceType;

public class MyConfig extends DefaultGenerationConfig {
    @Override
    public SourceType getSourceType() {
        return SourceType.JSON;
    }
}
