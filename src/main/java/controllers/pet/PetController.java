package controllers.pet;

import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import models.pet.PetModel;
import models.pet.PetNotFoundModel;
import org.hamcrest.Matchers;

import static io.restassured.RestAssured.given;

public class PetController {

    private static RequestSpecification requestSpecification;
    //    private static ResponseSpecification responseSpecification;
    private PetModel pet;

    public PetController(PetModel testPet) {

        requestSpecification = new RequestSpecBuilder()
                .addHeader("api_key", "api_key_api_key_api_key_api_key")
                .setBaseUri("http://petstore.swagger.io/v2")
                .setBasePath("pet")
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL).build();

        RestAssured.responseSpecification = new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .expectResponseTime(Matchers.lessThan(15000L))
                .build();

        RestAssured.defaultParser = Parser.JSON;
        this.pet = testPet;
    }

    @Step
    public PetModel addNewPet() {
        return given(requestSpecification)
                .body(pet)
                .post().as(PetModel.class);
    }

    @Step
    public void deletePet() {
        given(requestSpecification)
                .delete(String.valueOf(pet.getId()));
    }

    @Step
    public PetModel updatePet() {
        return given(requestSpecification)
                .body(pet)
                .put().as(PetModel.class);
    }

    /*public PetModel getPet() {
        return given(requestSpecification).get(String.valueOf(pet.getId()))
                .as(PetModel.class);
    }*/

    @Step
    public Object getPet() {
        Response response = given(requestSpecification).get(String.valueOf(pet.getId()));
        if (response.statusCode() == 200) {
            return response.as(PetModel.class);
        } else {
            return response.as(PetNotFoundModel.class);
        }
    }

    @Step
    public PetNotFoundModel getDeletedPet() {
        return given(requestSpecification)
                .get(String.valueOf(pet.getId()))
                .then()
                .statusCode(404)
                .and()
                .extract().response().as(PetNotFoundModel.class);
    }
}
