package models.pet;

import com.fasterxml.jackson.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "photoUrls",
        "tags",
        "status",
        "category"
})
public class PetModel {
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("photoUrls")
    private List<String> photoUrls = new ArrayList<>();
    @JsonProperty("tags")
    private List<TagsItem> tags = new ArrayList<>();
    @JsonProperty("status")
    private String status;
    @JsonProperty("category")
    private Category category;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public PetModel() {
    }

    public PetModel(List<String> photoUrls, String name, Integer id, Category category, List<TagsItem> tags, String status) {
        this.id = id;
        this.name = name;
        this.photoUrls = photoUrls;
        this.tags = tags;
        this.status = status;
        this.category = category;
    }

    private PetModel(Builder builder) {
        setPhotoUrls(builder.photoUrls);
        setName(builder.name);
        setId(builder.id);
        setCategory(builder.category);
        setTags(builder.tags);
        setStatus(builder.status);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("photoUrls")
    public List<String> getPhotoUrls() {
        return photoUrls;
    }

    @JsonProperty("photoUrls")
    public void setPhotoUrls(List<String> photoUrls) {
        this.photoUrls = photoUrls;
    }

    @JsonProperty("tags")
    public List<TagsItem> getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(List<TagsItem> tags) {
        this.tags = tags;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("category")
    public Category getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(Category category) {
        this.category = category;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(PetModel.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null) ? "<null>" : this.id));
        sb.append(',');
        sb.append("name");
        sb.append('=');
        sb.append(((this.name == null) ? "<null>" : this.name));
        sb.append(',');
        sb.append("photoUrls");
        sb.append('=');
        sb.append(((this.photoUrls == null) ? "<null>" : this.photoUrls));
        sb.append(',');
        sb.append("tags");
        sb.append('=');
        sb.append(((this.tags == null) ? "<null>" : this.tags));
        sb.append(',');
        sb.append("status");
        sb.append('=');
        sb.append(((this.status == null) ? "<null>" : this.status));
        sb.append(',');
        sb.append("category");
        sb.append('=');
        sb.append(((this.category == null) ? "<null>" : this.category));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null) ? "<null>" : this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.photoUrls == null) ? 0 : this.photoUrls.hashCode()));
        result = ((result * 31) + ((this.name == null) ? 0 : this.name.hashCode()));
        result = ((result * 31) + ((this.id == null) ? 0 : this.id.hashCode()));
        result = ((result * 31) + ((this.additionalProperties == null) ? 0 : this.additionalProperties.hashCode()));
        result = ((result * 31) + ((this.category == null) ? 0 : this.category.hashCode()));
        result = ((result * 31) + ((this.tags == null) ? 0 : this.tags.hashCode()));
        result = ((result * 31) + ((this.status == null) ? 0 : this.status.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PetModel) == false) {
            return false;
        }
        PetModel rhs = ((PetModel) other);
        return ((((((((this.photoUrls == rhs.photoUrls) || ((this.photoUrls != null) && this.photoUrls.equals(rhs.photoUrls))) && ((this.name == rhs.name) || ((this.name != null) && this.name.equals(rhs.name)))) && ((this.id == rhs.id) || ((this.id != null) && this.id.equals(rhs.id)))) && ((this.additionalProperties == rhs.additionalProperties) || ((this.additionalProperties != null) && this.additionalProperties.equals(rhs.additionalProperties)))) && ((this.category == rhs.category) || ((this.category != null) && this.category.equals(rhs.category)))) && ((this.tags == rhs.tags) || ((this.tags != null) && this.tags.equals(rhs.tags)))) && ((this.status == rhs.status) || ((this.status != null) && this.status.equals(rhs.status))));
    }

    public enum Status {

        AVAILABLE, UNAVAILABLE

    }

    public static final class Builder {
        private List<String> photoUrls;
        private String name;
        private Integer id;
        private Category category;
        private List<TagsItem> tags;
        private String status;

        private Builder() {
        }

        public Builder withPhotoUrls(List<String> val) {
            photoUrls = val;
            return this;
        }

        public Builder withName(String val) {
            name = val;
            return this;
        }

        public Builder withId(Integer val) {
            id = val;
            return this;
        }

        public Builder withCategory(Category val) {
            category = val;
            return this;
        }

        public Builder withTags(List<TagsItem> val) {
            tags = val;
            return this;
        }

        public Builder withStatus(String val) {
            status = val;
            return this;
        }

        public PetModel build() {
            return new PetModel(this);
        }
    }
}
