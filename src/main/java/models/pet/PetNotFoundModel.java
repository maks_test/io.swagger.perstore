package models.pet;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class PetNotFoundModel {

    @JsonProperty("code")
    private Integer code;

    @JsonProperty("type")
    private String type;

    @JsonProperty("message")
    private String message;

    public PetNotFoundModel() {
    }

    public PetNotFoundModel(Builder builder) {
        code = builder.code;
        type = builder.type;
        message = builder.message;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PetNotFoundModel that = (PetNotFoundModel) o;

        if (!getCode().equals(that.getCode())) return false;
        if (!getType().equals(that.getType())) return false;
        if (!getMessage().equals(that.getMessage())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = getCode().hashCode();
        result = 31 * result + getType().hashCode();
        result = 31 * result + getMessage().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return
                "PetNotFoundModel{" +
                        "code = '" + code + '\'' +
                        ",type = '" + type + '\'' +
                        ",message = '" + message + '\'' +
                        "}";
    }

    public static class Builder {
        private Integer code;
        private String type;
        private String message;

        public Builder setCode(Integer code) {
            this.code = code;
            return this;
        }

        public Builder setType(String type) {
            this.type = type;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public PetNotFoundModel build() {
            return new PetNotFoundModel(this);
        }
    }
}