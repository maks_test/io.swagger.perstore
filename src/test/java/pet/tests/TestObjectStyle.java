package pet.tests;

import assertions.pet.PetModelAssert;
import assertions.pet.PetNotFoundModelAssert;
import controllers.pet.PetController;
import models.pet.PetModel;
import models.pet.PetNotFoundModel;
import models.pet.TagsItem;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestObjectStyle {

    @Test
    public void testAddNewPetToStore() {
        List<TagsItem> tagsItemsList = new ArrayList<>();
        TagsItem tagsItem = new TagsItem();
        tagsItem.setId(1);
        tagsItem.setName("tag");
        TagsItem tagsItem2 = new TagsItem();
        tagsItem2.setId(2);
        tagsItem2.setName("tag2");
        tagsItemsList.add(tagsItem);
        tagsItemsList.add(tagsItem2);

        String testPetName = RandomStringUtils.randomAlphabetic(5);
        PetModel testPet = new PetModel(new ArrayList<>(), testPetName, 7676, null, tagsItemsList, PetModel.Status.AVAILABLE.name());
        PetModel petResponse = new PetController(testPet).addNewPet();
        PetModelAssert.assertThat(petResponse).isEqualTo(testPet);

        PetModelAssert.assertThat(petResponse).hasOnlyTags(tagsItem);
    }

    @Test
    public void updatePetTest() {
        String testPetName = RandomStringUtils.randomAlphabetic(5);
        int testPetId = Integer.valueOf(RandomStringUtils.randomNumeric(5));

        PetModel testPet = new PetModel(new ArrayList<>(), testPetName, testPetId, null, new ArrayList<>(), PetModel.Status.AVAILABLE.name());
        PetController petController = new PetController(testPet);
        petController.addNewPet();

        testPet.setStatus(PetModel.Status.UNAVAILABLE.name());

        PetModel petResponse = petController.updatePet();

        PetModelAssert.assertThat(petResponse).isEqualTo(testPet);
    }

    @Test
    public void deletePetTest() {
        String testPetName = RandomStringUtils.randomAlphabetic(5);
        int testPetId = Integer.valueOf(RandomStringUtils.randomNumeric(5));

        PetModel testPet = new PetModel(new ArrayList<>(), testPetName, testPetId, null, new ArrayList<>(), PetModel.Status.AVAILABLE.name());
        PetController petController = new PetController(testPet);
        petController.addNewPet();
        petController.deletePet();
        PetNotFoundModel petModel = (PetNotFoundModel) petController.getPet();

        System.out.println(petModel);

        PetNotFoundModelAssert.assertThat(petModel).isEqualTo(
                new PetNotFoundModel.Builder()
                        .setCode(1)
                        .setType("error")
                        .setMessage("Pet not found")
                        .build());
    }
}
