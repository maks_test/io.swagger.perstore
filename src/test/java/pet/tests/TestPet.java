package pet.tests;

import io.restassured.RestAssured;
import models.pet.PetModel;
import org.junit.Assert;
import org.junit.Test;


public class TestPet extends BaseTest {

    @Test
    public void postPetTest() {
        PetModel petModel = RestAssured
                .given(requestSpecification)
                .log().all()
                .body(testPet)
                .when()
                .post()
                .as(PetModel.class);
//                .then().extract().body().jsonPath().get("name").toString();

        Assert.assertEquals(testPet, petModel);

        //large-rafted-monkey-layer-hair
    }

    @Test
    public void getPetTest() {
        PetModel petModel = RestAssured.given(requestSpecification)
                .log().all()
                .get(testPet.getId().toString())
                .as(PetModel.class);
//                .then().body(Matchers.contains(testPet.getName()));

        Assert.assertEquals(testPet, petModel);
        //large-rafted-monkey-layer-hair
    }
}
