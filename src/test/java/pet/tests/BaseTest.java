package pet.tests;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import models.pet.PetModel;
import org.apache.commons.lang3.RandomStringUtils;
import utils.EndPoints;

import java.util.ArrayList;

class BaseTest {
    static final int id = Integer.valueOf(RandomStringUtils.randomNumeric(4));
    static final String name = RandomStringUtils.randomAlphabetic(5);
    static final PetModel testPet = new PetModel(new ArrayList<>(), name, id, null, new ArrayList<>(), "AVAILABLE");

    static RequestSpecification requestSpecification = new RequestSpecBuilder()
            .addCookie("coook", "adasd")
            .addHeader("api_key", "api_key_api_key_api_key_api_key")
            .setBaseUri(EndPoints.BASE_URI)
            .setBasePath(EndPoints.BASE_PATH)
            .setContentType(ContentType.JSON)
            .build();


/*    static RequestSpecification setRequestSpecification(){
        return new RequestSpecBuilder()
                .addCookie("coook", "adasd")
                .addHeader("api_key", "api_key_api_key_api_key_api_key")
                .setBaseUri(EndPoints.BASE_URI)
                .setBasePath(EndPoints.BASE_PATH)
                .setContentType(ContentType.JSON)
                .build();
    }*/
}
