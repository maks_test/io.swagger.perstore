package pet.tests;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import models.pet.PetModel;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import static io.restassured.config.EncoderConfig.encoderConfig;

public class TestPetOld {
    private static final String API_KEY = "api_key_api_key_api_key_api_key";
    private static final int PET_ID = 2;
    private static final String PET_NAME = "Mursik";
    private static final int CATEGORY_ID = 1;
    private static final String CATEGORY_NAME = "dogs";
    private static RequestSpecification requestSpec;

    @Test
    public void postPetTest() {
        requestSpec = new RequestSpecBuilder()
                .setBaseUri("http://petstore.swagger.io/v2/pet")
//                .setPort(8080)
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();

        PetModel petModel = RestAssured.given()
                .header("api_key", API_KEY)
                .spec(requestSpec)
                .config(RestAssured.config().encoderConfig(encoderConfig().encodeContentTypeAs("*/*", ContentType.JSON)))
                .body("{\n" +
                        "  \"id\": 1,\n" +
                        "  \"category\": {\n" +
                        "    \"id\": 10,\n" +
                        "    \"name\": \"string\"\n" +
                        "  },\n" +
                        "  \"name\": \"doggie\",\n" +
                        "  \"photoUrls\": [],\n" +
                        "  \"tags\": [],\n" +
                        "  \"status\": \"available\"\n" +
                        "}"
                )
                .when().post()
                .then()
                .statusCode(200)
//                .body("id", Matchers.equalTo(Integer.valueOf(PET_ID)));
                .extract().response().as(PetModel.class);

        Assert.assertEquals("pet name should be doggie", "doggie", petModel.getName());
        Assert.assertEquals("pet id should be 1", "1", petModel.getId().toString());
        Assert.assertEquals("pet status should be available", "available", petModel.getStatus());
    }

    @Test
    public void getPetTest() {
        RestAssured.given()
                .header("api_key", API_KEY)
                .spec(requestSpec)
                .basePath("1")
                .config(RestAssured.config().encoderConfig(encoderConfig().encodeContentTypeAs("*/*", ContentType.JSON)))
                .when().get()
                .then()
                .body("name", Matchers.equalTo("doggie"))
                .body("id", Matchers.equalTo(1))
                .body("status", Matchers.equalTo("available"))
                .extract().response().body()
                .prettyPrint();
    }
}
